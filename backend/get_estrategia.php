<?php
include 'connect_db.php';

	try {
		$sql = "SELECT * FROM estrategia ORDER BY descricao ASC";

		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		echo '<select id="id_estrategia" name="id_estrategia">';
		foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
			echo '<option value="'.$v['id'].'">'.$v['descricao'].'</option>';
		}
		echo '</select>';
	}	catch(PDOException $e)	{
		echo $sql . "<br>" . $e->getMessage();
	}
$conn=null;
?>