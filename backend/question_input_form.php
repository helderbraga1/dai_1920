<?php
echo '

<!--==========================
Questions Section
============================-->
<section id="questions"></section>
<section id="contact">
<div class="container wow fadeInUp">
  <div class="section-header">
	 <h3 class="section-title">Definir as coisas importantes da sua vida</h3>
	 <p class="section-description">Please answer the questions below!</p>
  </div>
</div>

<div class="container wow fadeInUp mt-5">
  <div class="row justify-content-center">
	 <div class="col-lg-5 col-md-8">
		<div class="form">
		  <div id="errormessage"></div>
		  <form action="backend/insert_answers.php" method="post" role="form" class="contactForm">
			 <div class="form-group">
				<input type="text" name="answer1" class="form-control" id="answer1" placeholder="O que é realmente importante na sua vida?" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer2" class="form-control" id="answer2" placeholder="Pergunta placeholder 2" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
			   <input type="email" name="email" class="form-control" id="email" placeholder="Email"/>
			   <div class="validation"></div>
			 </div>
			 <div class="text-center"><button type="submit">Send</button></div>
		  </form>
		</div>
	 </div>
  </div>
</div>
</section><!-- #questions -->

';?>