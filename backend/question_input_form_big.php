<?php
echo '

<!--==========================
Questions Section
============================-->
<section id="questions"></section>
<section id="contact">
<div class="container wow fadeInUp">
  <div class="section-header">
	 <h3 class="section-title">Identificar e Categorizar os seus problemas</h3>
	 <p class="section-description">Please answer the questions below!</p>
  </div>
</div>

<div class="container wow fadeInUp mt-5">
  <div class="row justify-content-center">
	 <div class="col-lg-5 col-md-8">
		<div class="form">
		  <div id="errormessage"></div>
		  <form action="backend/insert_answers_big.php" method="post" role="form" class="contactForm">
			 <div class="form-group">
				<input type="text" name="answer1" class="form-control" id="answer1" placeholder="O problema é:" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer2" class="form-control" id="answer2" placeholder="Possiveis soluções:" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer3" class="form-control" id="answer3" placeholder="A melhor solução é:" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer4" class="form-control" id="answer4" placeholder="O meu plano de implementar a solução:" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer5" class="form-control" id="answer5" placeholder="Plano executado?" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="text" name="answer6" class="form-control" id="answer6" placeholder="Problema resolvido?" data-rule="minlen:2" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="text-center"><button type="submit">Send</button></div>
		  </form>
		</div>
	 </div>
  </div>
</div>
</section><!-- #questions -->

';?>