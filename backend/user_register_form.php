<?php
echo '
<!--==========================
Register Section
============================-->
<section id="register">
<div class="container wow fadeInUp">
  <div class="section-header">
	 <h3 class="section-title">Register</h3>
	 <p class="section-description">Please give us your email to subscribe to our support program below. We are here to help!</p>
  </div>
</div>
<div class="container wow fadeInUp mt-5">
  <div class="row justify-content-center">
	 <div class="col-lg-5 col-md-8">
		<div class="form">
		  <div id="sendmessage">Your message has been sent. Thank you!</div>
		  <div id="errormessage"></div>
		  <form action="backend/insert_user.php" method="post" role="form" class="contactForm">
			 <div class="form-group">
				<input type="email" name="email" class="form-control" id="email" placeholder="Email" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
				<div class="validation"></div>
			 </div>
			 <div class="form-group">
				<input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:16" data-msg="Please enter a password of length between 16 to 256" />
				<div class="validation"></div>
			 </div>
			 <div class="text-center"><button type="submit">Send</button></div>
		  </form>
		</div>
	 </div>
  </div>
</div>
</section><!-- #register -->
</main>
';?>