<?php
echo '
<!--==========================
Activity Section
============================-->
<section id="activity"></section>
<section id="contact">
<div class="container wow fadeInUp">
	<div class="section-header">
		<h3 class="section-title">Alertas e Estrategias</h3>
	</div>
</div>
<div class="container wow fadeInUp mt-5">
	<div class="row justify-content-center">
		<div class="col-lg-5 col-md-8">
		<div class="form">
			<div id="errormessage"></div>
			<form action="backend/insert_alerta_estrategia.php" method="post" role="form" class="contactForm">
					<div class="form-group">
						<label>Alerta: </label>';
						include 'get_alerta.php';
echo '
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="alerta" name="alerta" placeholder="Descrição Alerta (Opcional)" />
					</div>
				<div class="form-group">
					<label>Estrategia: </label>';
					include 'get_estrategia.php';
echo '
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="observacao" name="observacao" placeholder="Descrição Estrategia (Opcional)" />
				</div>
				<div class="text-center"><button type="submit">Send</button></div>
			</form>
		</div>
		</div>
	</div>
</div>
</section><!-- #activity -->
</main>
';?>