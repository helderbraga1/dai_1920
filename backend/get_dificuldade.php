<?php
include 'connect_db.php';
//include 'backend/connect_db.php';

	try {
		$sql = "SELECT * FROM dificuldade";

		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		echo '<select id="dificuldade" name="dificuldade">';
		foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {

			echo '<option value="'.$v['id'].'">'.$v['nivel'].'</option>';
		}
		echo '</select>';
	}	catch(PDOException $e)	{
		echo $sql . "<br>" . $e->getMessage();
	}
$conn=null;
?>