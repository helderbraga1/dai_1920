<?php
include 'connect_db.php';
//include 'backend/connect_db.php';

	try {
		$sql = "SELECT * FROM tipo";

		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		echo '<select id="tipo" name="tipo">';
		foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {

			echo '<option value="'.$v['id'].'">'.$v['tipo'].'</option>';
		}
		echo '</select>';
	}	catch(PDOException $e)	{
		echo $sql . "<br>" . $e->getMessage();
	}
$conn=null;
?>