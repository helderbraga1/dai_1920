<?php
include 'connect_db.php';

	try {
		$sql = "SELECT * FROM acao ORDER BY descricao ASC";

		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		echo '<select id="id_descricao" name="id_descricao">';
//		echo '<div class="btn-group"><button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Descrição</button><div class="dropdown-menu dropdown-menu-right" id="id_descricao" name="id_descricao">';
		foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
			echo '<option value="'.$v['id'].'">'.$v['descricao'].'</option>';
//			echo '<button class="dropdown-item" type="button" value="'.$v['id'].'">'.$v['descricao'].'</button>';
		}
		echo '</select>';
//		echo ' </div></div>';
	}	catch(PDOException $e)	{
		echo $sql . "<br>" . $e->getMessage();
	}
$conn=null;
?>