<?php
echo '
<!--==========================
Activity Section
============================-->
<section id="activity"></section>
<section id="contact">
<div class="container wow fadeInUp">
	<div class="section-header">
		<h3 class="section-title">Etapa I - Ativação Comportamental</h3>
	</div>
</div>
<div class="container wow fadeInUp mt-5">
	<div class="row justify-content-center">
		<div class="col-lg-5 col-md-8">
		<div class="form">
			<div id="errormessage"></div>
			<form action="backend/insert_activity.php" method="post" role="form" class="contactForm">
					<div class="form-group">
						<label>Descrição: </label>';
						include 'get_action.php';
echo '
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição (Opcional)" />
					</div>
				<div class="form-group">
					<label>Tipo: </label>';
					include 'get_tipo.php';
echo'			</div>
				<div class="form-group">
					<label>Dificuldade: </label>';
					include 'get_dificuldade.php';
echo '
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="observacao" name="observacao" placeholder="Observação" />
				</div>
				<div class="text-center"><button type="submit">Send</button></div>
			</form>
		</div>
		</div>
	</div>
</div>
</section><!-- #activity -->
</main>
';?>