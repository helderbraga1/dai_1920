<?php
include "backend/connect_db.php";

try{
	$sql = "SELECT id, title, start_date, end_date FROM events LIMIT 20";
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	// set the resulting array to associative
	$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
	$calendar = array();
	foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$rows) {
		// convert date to milliseconds
		$start = strtotime($rows['start_date']) * 1000;
		$end = strtotime($rows['end_date']) * 1000;
		$calendar[] = array(
		'id' =>$rows['id'],
		'title' => $rows['title'],
		'url' => "#",
		"class" => 'event-important',
		'start' => "$start",
		'end' => "$end"
		);
		}
		$calendarData = array(
		"success" => 1,
		"result"=>$calendar);
		echo json_encode($calendarData);
		
} catch(PDOException $e) {
	echo $sql . "<br>" . $e->getMessage();
}
$conn=null;

?>