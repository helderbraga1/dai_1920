<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Helping Hand</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Regna
    Theme URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->


	<!-- =======================================================
		Calendar code
	======================================================= -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/calendar.css">

</head>

<body>
    <!--==========================
    Header
    ============================-->
    <header id="header">
      <div class="container">
        <div id="logo" class="pull-left">
          <a href="#hero"><img src="img/logo.png" alt="Helping Hand" title="" style='max-width:10%;max-height:10%;opacity:30%'/></a>
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu">
            <li class="menu-active"><a href="#hero">Home</a></li>
            <li><a href="#facts">Facts</a></li>
            <li><a href="#activity">Etapa I</a></li>
            <li><a href="#contact">Etapa II</a></li>
            <?php
if(!isset($_SESSION["id_user"])){
  $_SESSION["id_user"]=null; 
  echo '<li><a href="#questions">Etapa III</a></li>';
} else {
  echo '<li><a href="#register">Newsletter</a></li>';            
}          
?>

            <li><a href="#about">About Us</a></li>
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
    </header><!-- #header -->

    <!--==========================
      Hero Section
    ============================-->
    <section id="hero">
      <div class="hero-container">
        <h1>Welcome to Helping Hand</h1>
        <h2>Your daily helper in fighting depression.</h2>
        <a href="#questions" class="btn-get-started">Get Started</a>
      </div>
    </section><!-- #hero -->

    <main id="main">
      <!--==========================
        Facts Section
      ============================-->
      <section id="facts">
        <div class="container wow fadeIn">
          <div class="section-header">
            <h3 class="section-title">Facts</h3>
            <p class="section-description">Depression in numbers</p>
          </div>
          <div class="row counters">

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">75</span>
              <p>percent of people with minor depression will develop a more acute variant within 5 years<a href=https://www.health.harvard.edu/healthbeat/feeling-down-it-could-be-low-level-depression>[1]</a></p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">20</span>
              <p>percent of people in the US reported sufering for at least one type of depression<a href="http://www.daveneefoundation.org/scholarship/facts-about-depression-and-suicide/">[2]</a></p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">322</span>
              <p>million people worldwide suffer with depression<a href="https://adaa.org/understanding-anxiety/depression">[3]</a></p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">17</span>
              <p>percent of people in the US suffer from a form of depression<a href="https://emedicine.medscape.com/article/805459-overview">[4]</a></p>
            </div>
          </div>
        </div>
      </section><!-- #facts -->


    <!--==========================
      Call To Action Section
    ============================-->
      <section id="call-to-action">
        <div class="container wow fadeIn">
          <div class="row">
            <div class="col-lg-9 text-center text-lg-left">
              <h3 class="cta-title">Call To Action</h3>
              <p class="cta-text"> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
            <div class="col-lg-3 cta-btn-container text-center">
              <a class="cta-btn align-middle" href="#">Call To Action</a>
            </div>
          </div>
        </div>
      </section><!-- #call-to-action -->

      <?php
      // define variables and set to empty values    
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        if (empty($_POST["email"])) {
          $_POST["email"] = null;
        }  
      }

      if(!isset($_SESSION["id_user"])) { 
        require 'backend/activity_input_form.php';}
?>

<section id="calendar">
      <div class="container">
        <div class="page-header">
          <div class="pull-right form-inline">
            <div class="btn-group">
              <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
              <button class="btn btn-default" data-calendar-nav="today">Today</button>
              <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
            </div>
            <div class="btn-group">
              <button class="btn btn-warning" data-calendar-view="year">Year</button>
              <button class="btn btn-warning active" data-calendar-view="month">Month</button>
              <button class="btn btn-warning" data-calendar-view="week">Week</button>
              <button class="btn btn-warning" data-calendar-view="day">Day</button>
            </div>
        </div>
        <h3></h3>
        </div>
          <div class="row">
            <div class="col-md-9">
              <div id="showEventCalendar"></div>
              </div>
              <div class="col-md-3">
                <h4>All Events List</h4>
                  <ul id="eventlist" class="nav nav-list"></ul>
              </div>
          </div>
      </div>
        </section>

<h2>Etapa II - Resolução de Problemas</h2>
<?php
      if(!isset($_SESSION["id_user"])) { 

        
        require 'backend/question_input_form.php';
        require 'backend/question_input_form_big.php';
      } else {
        require 'backend/user_register_form.php'; 
      }

      ?>
      <h2>Etapa III - Prevenção da recaída</h2>
      <?php
      require 'backend/alerta_estrategia_input_form.php';
      
      require 'backend/acontecimento_estrategia_input_form.php';

      ?>
      <br>
    </main>
    <!--==========================
      Footer
    ============================-->
    <footer id="footer">
      <div class="footer-top">
        <div class="container">
        </div>
      </div>

      <div class="container">
        <div class="copyright">
        &copy; Copyright <strong>Regna</strong>. All Rights Reserved
        </div>
        <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Regna
        -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/jquery/jquery-migrate.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/superfish/hoverIntent.js"></script>
    <script src="lib/superfish/superfish.min.js"></script>

    <!-- Contact Form JavaScript File -->
    <script src="contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="js/main.js"></script>

    <!-- =======================================================
      Calendar code
    ======================================================= -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script type="text/javascript" src="js/calendar.js"></script>
    <script type="text/javascript" src="js/events.js"></script>

  </body>
</html>
